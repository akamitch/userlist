package com.example.userlist.repository;

import com.example.userlist.model.User;

import java.sql.*;

/*
CREATE TABLE userlist (
    id                      SERIAL,
    username                varchar(10),
    password                varchar(20),
    accountNonExpired       boolean,
    accountNonLocked        boolean,
    credentialsNonExpired      boolean,
    enabled                    boolean
);
INSERT INTO userlist (username, password, accountNonExpired, accountNonLocked, credentialsNonExpired, enabled)
VALUES ('sam', 'Yojimbo', 'true', 'true', 'true', 'true');

select * from userlist;
*/

//implements UserRepository
public class UserRepositoryPostgress {
    //  Database credentials
    static final String DB_URL = "jdbc:postgresql://193.182.144.149:5432/userlist";
    //jdbc:postgresql://host:port/database
    static final String USER = "userlist";
    static final String PASS = "H2H3h1dyyu8sd";

    public static User saveUser(User user) throws SQLException, ClassNotFoundException {
        Connection connection = connectDb();
        Statement statement = null;

        statement = connection.createStatement();
        String query = "INSERT INTO userlist.userlist (username, password, accountnonexpired, accountnonlocked, credentialsnonexpired, enabled) VALUES ('fromjava', 'dfdfgbo', 'true', 'true', 'true', 'true');";
        //statement.executeUpdate(query);

        String SQL = "CREATE TABLE developers " +
                "(id INTEGER not NULL, " +
                " name VARCHAR(50), " +
                " specialty VARCHAR (50), " +
                " salary INTEGER not NULL, " +
                " PRIMARY KEY (id))";

        statement.executeUpdate(SQL);




        return user;
    }

    private static Connection connectDb() throws ClassNotFoundException {
        Connection connection = null;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);

        } catch (SQLException e) {
            System.out.println("Connection to DB Failed");
            e.printStackTrace();
        }

        return connection;
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException{

        saveUser(new User());
    }
//    public static void main(String[] argv) {
//        //Test connection to DB
//
//        System.out.println("Testing connection to PostgreSQL JDBC");
//
//        try {
//            Class.forName("org.postgresql.Driver");
//        } catch (ClassNotFoundException e) {
//            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
//            e.printStackTrace();
//            return;
//        }
//
//        System.out.println("PostgreSQL JDBC Driver successfully connected");
//        Connection connection = null;
//
//        try {
//            connection = DriverManager
//                    .getConnection(DB_URL, USER, PASS);
//
//        } catch (SQLException e) {
//            System.out.println("Connection Failed");
//            e.printStackTrace();
//            return;
//        }
//
//        if (connection != null) {
//            System.out.println("You successfully connected to database now");
//        } else {
//            System.out.println("Failed to make connection to database");
//        }
//    }

}
